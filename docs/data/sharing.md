# Data sharing

##Security and Data Integrety

Sharing data with other users must be done carefully. Permissions
should be set to the minimum necessary to achieve the desired
access. For instance, consider carefully whether it's really necessary
before sharing write permssions on data. Be sure to have archived
backups of any critical shared data. It is also important to ensure
that private login secrets (like SSH private keys or apache
htaccess files) are not shared with other users (either intentionally
or accidentally). Good practice is to keep things like this in a
separare directory that is as locked down as possible.

## Sharing Data within NERSC

### Sharing with Other Members of Your Project

NERSC's [Community file system](../filesystems/community.md) is set up
with group read and write permissions and is ideal for sharing with
other members of your project. There is a directory for every active
project at NERSC and all members of that project should have access to
it by default.

### Sharing with NERSC Users Outside of Your Project

#### Unix File Permissions

You can share files and directories with NERSC users outside of your
project by adjusting the unix file permissions. We have an extensive
write up of unix file permissions and how they work
[here](../filesystems/unix-file-permissions.md).

#### Give/Take

NERSC provides two commands: `give` and `take` which are useful for
sharing small amounts of data between users.

To send a file or path to `<receiving_username>`:
```
nersc$ give -u <receiving_username> <file or directory>
```

To receive a file sent by `<sending_username>`:
```
nersc$ take -u <sending_username> <filename>
```

To take all files from `<sending_username>`:
```
nersc$ take -a -u <sending_username>
```

To see what files `<sending_username>` has sent to you:
```
nersc$ take -u <sending_username>
```

For a full list of options pass the `--help` flag.

!!! warning
    Files that remain untaken 12 weeks after being given will be
    purged from the staging area.

## Sharing Data outside of NERSC

You can easily and quickly share data over the web using our [Science
Gateways](../services/science-gateways.md) framework.

You can also share large volumes of data externally by setting up a
[Globus Sharing Endpoint](../../services/globus/#globus-sharing).
