# Welcome to NERSC

Welcome to the National Energy Research Scientific Computing Center,
a high performance scientific computing center. This document will
guide you through the basics of using NERSC's supercomputers, storage
systems, and services.

## What is NERSC?

NERSC provides High Performance Computing and Storage facilities
and support for research sponsored by, and of interest to, the U.S.
Department of Energy Office of Science. NERSC has the unique
programmatic role of supporting all six Office of Science program
offices: Advanced Scientific Computing Research, Basic Energy
Sciences, Biological and Environmental Research, Fusion Energy
Sciences, High Energy Physics, and Nuclear Physics. Scientists who
have been awarded research funding by any of the offices are eligible
to apply for an allocation of NERSC time. Additional awards may be
given to non-DOE funded project teams whose research is aligned
with the Office of Science's mission. Allocations of time and storage
are made by DOE.

NERSC has about 7,000 active user accounts from across the U.S. and
internationally.

NERSC is a national center, organizationally part of [Lawrence
Berkeley National Laboratory](http://www.lbl.gov) in Berkeley, CA.
NERSC staff and facilities are primarily located at Berkeley Lab's
Shyh Wang Hall on the Berkeley Lab campus.

## Computing & Storage Resources

### Cori

[Cori](../systems/cori/index.md) is a Cray XC40 with 76,416 compute
cores of Intel Xeon ("Haswell") and 658,784 compute cores of Intel
Xeon Phi ("Knights Landing"). The Xeon nodes have a total of 298.5 TB
of memory, and the Xeon Phi nodes have a total of nearly 1.1 PB of
memory. Cori has a 30 PB of disk-based local filesystem, 1.8 PB of
flash-based storage in a burst buffer, and features the Cray "Aries"
high-speed internal network.

### Community File System (CFS)

The [Community File System (CFS)](../filesystems/community.md)
provides permanent storage to groups of users who want to share
data. Quotas on the file system are determined by DOE Program
Managers based on information PI supply in their yearly allocation
request. CFS is available from all NERSC compute systems.

### HPSS Archival Storage

NERSC's archival storage system provides up to 240 PB of permanent,
archival data storage.

## New NERSC Accounts

In order to use the NERSC facilities, you need:

1. Access to an allocation of computational or storage resources as a
   member of a project
2. A user account with an associated user login name (also called
   username).

If you are not a member of a project that already has a NERSC award,
you may apply for an allocation. If you need to get a new user
account that will be associated with an existing NERSC award, you
should submit a request for a new NERSC account.

-  [User Accounts](../accounts/index.md)
-  [NERSC Allocations](https://www.nersc.gov/users/accounts/allocations/)

### Passwords

Each person has a single password associated with their login
account. This password is used to access all NERSC resources. As a new
user, you will receive an email with a link to set your initial
password. You should also answer the security questions; this will
allow you to reset your password yourself should you forget it.

-  [Passwords](../accounts/passwords.md)

### Multi-Factor Authentication (MFA)

NERSC users are required to use the second "factor," besides the
password, for logging into NERSC resources, for better cyber security
and protection of your data. For that, users have to create a MFA
token in Iris and configure it on an authenticator app on a mobile
device to generate a one-time password (OTP). Users then enter
both password and OTP to authenticate to NERSC resources. For
detailed info on how to generate a MFA token in Iris, configure it
and use MFA can be found in the [MFA webpage](../connect/mfa.md).

### Login Failures

If you accrue too many consecutive login failures when accessing a
NERSC system, your account on that system will be locked.  To clear
these failed logins, log in to [Iris](http://iris.nersc.gov/),
and unlock your account in the 'Self-service User Info' menu section.

-  [Additional details for login failures](../iris/iris-for-users/#details).

## NERSC Users Group (NUG)

Join the [NERSC Users Group](https://www.nersc.gov/users/NUG/): an
independent organization of users of NERSC resources.

!!! tip
	NUG maintains a Slack workspace that all users are welcome to
	[join](https://www.nersc.gov/users/NUG/nersc-users-slack/).

## Accounting Web Interface (Iris)

You log into the Iris web site at
[https://iris.nersc.gov/](https://iris.nersc.gov/) to manage your
NERSC accounts. In Iris, you can check your daily allocation balances,
change your password, run reports, update your contact information,
change your login shell, etc.  See [Iris Web
Portal](../iris/iris-for-users.md).

## How to Get Help

With an emphasis on enabling science and providing user-oriented
systems and services, NERSC encourages you to ask lots of questions.
There are lots of way to do just that.

Your primary resources are the NERSC main web site, [NERSC documentation
site](../index.md) and the HPC Consulting and Account Support staff.
The consultants can be contacted by phone, email, or the [web
ticketing system](https://help.nersc.gov/) during working hours
Pacific Time. NERSC's consultants are HPC experts and can answer
just about all of your questions.

The NERSC web site is always available with a rich set of documentation,
tutorials, and [live status](https://my.nersc.gov) information.

Technical questions, computer operations, passwords, and account
support

-  Online Help Desk: [help.nersc.gov](https://help.nersc.gov/) (authentication required)
-  Account Support: [help.nersc.gov](https://help.nersc.gov/) (authentication required), or accounts@nersc.gov
-  HPC Consulting: [help.nersc.gov](https://help.nersc.gov/)
-  Computer Operations (for urgent machine issues only):
1-800-666-3772 (USA only) or 1-510-486-8600, Option 1.

Contact computer operations (24x7) to report machine issues. Account Support and
HPC Consulting are available 8-5 Pacific Time on business days.

-  [Contacting NERSC](../help/index.md)
-  [Connecting to NERSC](../connect/login-nodes.md)
-  [Documentation](../index.md)

## Software

NERSC and our vendors supply a rich set of HPC utilities,
applications, and programming libraries. If there is something missing
that you would like to have on our systems, please submit a request
on [help.nersc.gov](http://help.nersc.gov/) and we will evaluate it
for appropriateness, cost, effort, and benefit to the community.

-  [NERSC Supported Software Status List](../policies/software-policy/software_state.md).

## Computing Environment

When you log in to any NERSC computer (not HPSS), you are in your
[global `$HOME` directory](../filesystems/global-home.md). You
initially land in the same place no matter what machine you connect
to. This means that if you have files or binary executables that
are specific to a certain system, you need to manage their location.

-  [Customizing your environment](../environment/index.md)

### Compiler Wrappers

NERSC provides compiler wrappers which combine the native compilers
(Intel, GNU, and Cray) with MPI and various other libraries, to
enable streamlined compilation of scientific applications. Users
are recommended to use the Cray compiler wrappers `cc`, `CC`, and
`ftn` for building C, C++, and Fortran codes, respectively. The
Intel compiler wrappers, `mpicc`, `mpiicpc`, and `mpiifort`, are
also provided for using Intel MPI.

-  [details for compilers at NERSC](../programming/compilers/wrappers.md)

## Running Jobs

Typical usage of the system involves submitting scripts (also
referred to as "jobs") to a batch system such as
[Slurm](https://slurm.schedmd.com/).

-  [overview of jobs at NERSC](../jobs/index.md)
-  [rich set of examples](../jobs/examples/index.md)

## Transferring data

We provide several ways for transferring data both inside and outside
NERSC. To transfer files from/to NERSC, we suggest using the dedicated
[Data Transfer Nodes](../systems/dtn/index.md), which are optimized
for bandwidth and have access for most of the NERSC file systems.

-  [Transferring Data](../data/transfer.md)

## Archiving Files with HPSS

The [High Performance Storage System (HPSS)](../filesystems/archive.md)
is a modern, flexible, performance-oriented mass storage system.
It has been used at NERSC for archival storage since 1998. It is a
valuable resource for permanently archiving user's data.

-  [HPSS documentation webpage](../filesystems/archive.md)
