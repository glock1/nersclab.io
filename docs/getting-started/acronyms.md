# Acronyms

| Acronym | Term  |
|:--------|:------|
| ALCC  | ASCR Leadership Computing Challenge |
| ASCR  | [Advanced Scientific Computing Research (office of)](https://science.osti.gov/ascr) |
| ASIC  | Application-Specific Integrated Circuit |
| CPU   | Central Processing Unit |
| CRT   | Computational Research and Theory Facility (aka Wang Hall) [map](https://goo.gl/maps/mJYcYDE1rLFQFBsv6) |
| DTN   | Data Transfer Node |
| DVS   | Data Virtualization Service |
| ERCAP | Energy Research Computer Allocations Program |
| JGI   | [Joint Genome Institute](https://jgi.doe.gov) |
| GPFS  | (IBM's) General Purpose File System |
| GPU   | Graphical Processing Unit |
| HPC   | High Performance Computing |
| HPSS  | High Performance Storage System |
| HT (also HTT) | Hyperthreading |
| IO (also I/O) | Input Output |
| IP    | [Internet Protocol](https://tools.ietf.org/html/rfc791) |
| KNL   | Knights Landing |
| LDAP  | [Lightweight Directory Access Protocol](https://tools.ietf.org/html/rfc4511) |
| MCDRAM | Multi-Channel DRAM |
| MFA   | Multi-Factor Authentication |
| MKL   | (Intel) [Math Kernel Library](https://software.intel.com/en-us/mkl) |
| MOTD  | [Message of the Day](https://www.nersc.gov/live-status/motd/) |
| MOM   | Machine Oriented Mini-server |
| MPI   | [Message Passing Interface](https://www.mpi-forum.org) |
| MPMD  | Multiple Programs, Multiple Data |
| MPP   | Massively Parallel Processor |
| MTBF  | Mean Time Between Failures |
| NERSC | [National Energy Research Scientific Computing](https://www.nersc.gov) |
| NGF   | NERSC Global File system |
| NUG   | [NERSC User Group](https://www.nersc.gov/users/NUG/) |
| NUMA  | Non-Uniform Memory Access |
| OS    | Operating System |
| OTP   | One-time Password |
| PGI   | Portland Group Incorporated |
| PI    | Principal Investigator |
| QDR   | Quad Data Rate |
| QOS   | Quality of Service |
| SIMD  | Single Instruction, Multiple Data |
| SIMT  | Single Instruction, Multiple Threads |
| SPMD  | Single Program, Multiple Data |
| SSH   | Secure Shell |
| UPC   | Unified Parallel C (programming language) |
