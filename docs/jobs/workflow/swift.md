#Swift Overview

The Swift scripting language provides a simple, compact way to write parallel
scripts that run many copies of ordinary programs concurrently in various
workflow patterns, reducing the need for complex parallel programming or arcane
scripting. Swift is very general, and is in use in domains ranging from earth
systems to bioinformatics to molecular modeling.

#Access

Type the following commands to run a simple Swift script:

```
% module load swift
% swift -config swift.conf myscript.swift
```

# Swift Tutorial on NERSC Systems

This [site](http://swift-lang.org/swift-tutorial/doc/tutorial.html)
has a set of excellent introductory tutorials for Swift that run on
NERSC systems. On Cori, just remember to type "module load
swift" before starting the tutorials.

An introductory talk by Michael Wilde was given at NERSC in December 2015. The
slides from this seminar can be found
[here](https://www.nersc.gov/assets/Uploads/NERSC.Swift.Overview.2015.1201.pdf).


