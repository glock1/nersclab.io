# Cori PE Software Default History

This page lists the PE software (Cray CDT and Compilers) default set on Cori in reverse order.

##Jan 14, 2020
  
  Programming Environment changes including new default versions (CDT/19.11) after scheduled maintenance. Detailed lists can be found [here](./2019Dec-2020Jan.md). 

  python/3.7-anaconda-2019.10 is set as the default version.  

##Jul 27-30, 2019   

  OS upgrade from CLE6.0UP07 to CLE7.0UP00. 

  Programming Environment changes including new default versions (CDT/19.03 and intel/19.0.3.199), old software removals (CDT/18.03, CDT/18.09. CDT/18.12), and new software available (CDT/19.06) and the craype-hugepages2M module loaded by default after scheduled maintenance. Detailed lists can be found [here](./2019Jul.md). 

##Jan 8, 2019    

  Programming Environment changes including new default versions (CDT/18.09), old software removals (CDT/18.06), and new software available (CDT/18.12) after scheduled maintenance. Detailed lists can be found [here](./2019Jan.md). 

##Jul 11, 2018    

  Programming Environment changes including new default versions (CDT/18.03), old software removals (CDT/17.06), and new software available (CDT/18.06) after scheduled maintenance. Detailed lists can be found [here](./2018Jul.md).  

##Jan 9, 2018

 Slurm upgrade from 17.02 to 17.11. New QOS only submission scheme in place. Reservations required for non-quad,cache jobs.

 Intel default version changed from 18.0.0.128 to 18.0.1.163.

 SW default change to CDT 17.09 after scheduled maintenance: 
 
* cray-libsci/17.09.1 (from 17.06.1)
* cray-mpich/7.6.2 (from 7.6.0)
* cray-shmem/7.6.2 (from 7.6.0)
* papi/5.5.1.3 (from 5.5.1.2)
* perftools, perftools-base, perftools-lite/6.5.2 (from 6.5.0)
* cce/8.6.2 (from 8.6.1)
* gcc/7.1.0 (from 6.3.0)
* stat/3.0.1.1 (from 3.0.1.0)
* cray-python/17.09.1 (from 17.06.1)
* cray-lgdb/3.0.7 (from 3.0.6)
* cray-mpich-abi/7.6.2 (from 7.6.0)

##Oct 6-9, 2017

  Intel default version changed from 17.0.2.174 to 18.0.0.128.

##Aug 8-10, 2017    

  OS upgrade from CLE6.0UP03 to CLE6.0UP04.  

  SW default change to CDT 17.06 after scheduled maintenance: 

* cray-libsci/17.06.1 (from 16.09.1)
* cray-mpich/7.6.0 (from 7.4.4)
* cray-shmem/7.6.0 (from 7.4.4)
* craype/2.5.12 (from 2.5.7)
* cray-fftw/3.3.6.2 (from 3.3.4.10)
* papi/5.5.1.2 (from 5.5.1.1)
* perftools, perftools-base, perftools-lite/6.5.0 (from 6.4.6)
* cce/8.6.1 (from 8.5.4)
* gcc/6.3.0 (from 6.2.0)
* atp/2.1.1 (from 2.0.3)
* stat/3.0.1.0 (from 2.2.0.3)
* cray-ccdb/3.0.3 (from 2.0.3)
* cray-lgdb/3.0.6 (from 3.0.4)
* cray-netcdf/4.4.1.1.3 (from 4.4.1) 
* cray-netcdf-hdf5parallel/4.4.1.1.3 (from 4.4.1) 
* cray-petsc, cray-petsc-complex, cray-petsc-complex-64/3.7.6.0 (from 3.7.2.1) 
* cray-tpsl, cray-tpsl-64/17.06.1 (from 16.07.1)
* cray-trilinos/12.10.1.1 (from 12.6.3.3)
* cray-cti/1.0.6 (from 1.0.3)
* cray-mpich-abi/7.6.0 (from 7.4.4)
* craypkg-gen/1.3.5 (from 1.3.4)
* modules/3.2.10.6 (from 3.2.10.5)

##Nov 19, 2016

  SW default change to CDT 16.10 after scheduled maintenance:
  
* cray-ga/5.3.0.7 (from 5.3.0.6)
* cray-libsci/16.09.1 (from 16.06.1)
* cray-mpich/7.4.4 (from 7.4.0)
* cray-shmem/7.4.4 (from 7.4.0)
* craype/2.5.7 (from 2.5.5)
* fftw/3.3.4.10 (from 3.3.4.8)
* papi/5.4.3.3 (from 5.4.3.2)
* perftools, perftools-base, perftools-lite/6.4.2 (from 6.4.0)
* cce/8.5.4 (from 8.5.0)
* gcc/6.2.0 (from 5.3.0)
* atp/2.0.3 (from 2.0.2)
* cray-ccdb/2.0.3 (from 2.0.1)
* cray-lgdb/3.0.4 (from 3.0.2)
* cray-netcdf/4.4.1 (from 4.4.0) 
* cray-netcdf-hdf5parallel/4.4.1 (from 4.4.0) 
* cray-petsc, cray-petsc-complex, cray-petsc-complex-64/3.7.2.1 (from 3.7.0.0) 
* cray-tpsl, cray-tpsl-64/16.07.1 (from 16.06.1)
* cray-trilinos/12.6.3.3 (from 12.6.3.0)
* cray-cti/1.0.3 (from 1.0.1)
* cray-mpich-abi/7.4.4 (from 7.4.0)
* craypkg-gen/1.3.4 (from 1.3.3)
* modules/3.2.10.5 (from 3.2.10.4)

##Jun 13 - 30, 2016

  OS upgrade to Rhine/Redwood (CLE6) to prepare for KNL arrival.

  SW default change to CDT 16.06 after CRT power outage:

* cray-ga/5.3.0.6 (from 5.3.0.6)
* cray-libsci/16.06.1 (from 13.3.0)
* cray-mpich/7.4.0 (from 7.3.1)
* cray-shmem/7.4.0 (from 7.3.1)
* craype/2.5.5 (from 2.5.1)
* fftw/3.3.4.8 (from 3.3.4.6)
* papi/5.4.3.2 (from 5.4.1.3)
* perftools, perftools-lite/6.4.0 (from 6.3.1)
* cce/8.5.0 (from 8.4.3)
* gcc/5.3.0 (from 5.2.0)

##Feb 27, 2016

  SW default change to CDT 16.01 after CRT power outage:

* pmi/5.0.10-1.0000.11050.0.0.ari (from 5.0.9-1.0000.10911.0.0.ari)
* cray-ga/5.3.0.5 (from 5.3.0.3)
* cray-hdf5, cray-hdf5-parallel/1.8.16 (from 1.8.14)
* cray-libsci/13.3.0 (from 13.2.0)
* cray-mpich/7.3.1 (from 7.2.5)
* cray-shmem/7.3.1 (from 7.2.5)
* craype/2.5.1 (from 2.4.2)
* fftw/3.3.4.6 (from 3.3.4.5)
* papi/5.4.1.3 (from 5.4.1.2)
* perftools, perftools-lite/6.3.1 (from 6.3.0)
* cce.8.4.3 (from 8.4.0)
* gcc/5.2.0 (from 5.1.0)
