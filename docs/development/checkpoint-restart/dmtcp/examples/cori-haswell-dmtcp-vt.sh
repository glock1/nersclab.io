#!/bin/bash 
#SBATCH -J test
#SBATCH -q regular 
#SBATCH -N 1             
#SBATCH -C haswell
#SBATCH --time=48:00:00 
#SBATCH --error=test%j.err 
#SBATCH --output=test%j.out
#SBATCH --time-min=06:00:00  
#
#SBATCH --comment=48:00:00
#SBATCH --signal=B:USR1@300
#SBATCH --requeue
#SBATCH --open-mode=append

#for c/r jobs
module load dmtcp nersc_cr

#checkpointing once every hour
start_coordinator -i 3600

#c/r jobs
if [[ $(restart_count) == 0 ]]; then

    #user setting
    export OMP_PROC_BIND=true
    export OMP_PLACES=threads
    export OMP_NUM_THREADS=32
    dmtcp_launch -j ./a.out &
elif [[ $(restart_count) > 0 ]] && [[ -e dmtcp_restart_script.sh ]]; then

    ./dmtcp_restart_script.sh &
else

    echo "Failed to restart the job, exit"; exit
fi

# requeueing the job if remaining time >0
ckpt_command=ckpt_dmtcp    #additional checkpointing right before the job hits the walllimit 
requeue_job func_trap USR1

wait

